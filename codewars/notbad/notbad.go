// Problem 6: not_bad
// Given a string, find the first appearance of the
// substring 'not' and 'bad'. If the 'bad' follows
// the 'not', replace the whole 'not'...'bad' substring
// with 'good'.
// Return the resulting string.
// So 'This dinner is not that bad!' yields:
// This dinner is good!

package main

import (
	"fmt"
	"strings"
)

func notBad(s string) string {
	not := strings.Index(s, "not")
	bad := strings.Index(s, "bad")
	if bad > not {
		return s[:not] + "good" + s[bad+3:]
	}
	return s
}

func main() {
	fmt.Println(notBad("This movie is not so bad"))
	fmt.Println(notBad("This dinner is not that bad!"))
	fmt.Println(notBad("This tea is not hot"))
	fmt.Println(notBad("It's bad yet not"))
}
