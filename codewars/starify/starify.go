// Problem 3
// Given a string s, return a string
// where all occurrences of its first char have
// been changed to '*', except do not change
// the first char itself.
// e.g. 'babble' yields 'ba**le'
// Assume that the string is length 1 or more.

package main

import (
	"fmt"
)

func starify(s string) {
	firstChar := s[0]
	fmt.Printf("%c", firstChar)
	for char := 1; char < len(s); char++ {
		if s[char] == firstChar {
			fmt.Printf("*")
		} else {
			fmt.Printf("%c", s[char])
		}
	}
	fmt.Println()
}

func main() {
	starify("babble")
	starify("aardvark")
	starify("google")
	starify("donut")
}
