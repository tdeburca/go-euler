package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func firstline(filename string) {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	//scanner.Split(bufio.ScanWords)
	if scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func main() {
	filePtr := flag.String("filename", "alice.txt", "The file to operate on")
	firstlinePtr := flag.Bool("firstline", false, "Print the first line of the file")
	numberoflinesPtr := flag.Bool("numberoflines", false, "Print the number of lines in the file.")
	numberofwordsPtr := flag.Bool("numberofwords", false, "Print the number of words in the file.")
	wordcountPtr := flag.Bool("wordcount", false, "Show the frequency of each word.")
	topcountPtr := flag.Bool("topccount", false, "Show the 20 most frequent words.")

	// better looking declaration:
	// var ip = flag.Int("flagname", 1234, "help message for flagname")

	flag.Parse()
	fmt.Println("filename", filePtr)
	fmt.Println("firstlne", *firstlinePtr)
	fmt.Println("numberoflines", *numberoflinesPtr)
	fmt.Println("numberofwords", *numberofwordsPtr)
	fmt.Println("wordcount", *wordcountPtr)
	fmt.Println("topcount", *topcountPtr)

	if !filePtr {
		fmt.Println("File name required")
		os.Exit(1)
	}

	if !*firstlinePtr {
		firstline(*filePtr)
	}

}
