// Problem 3
// The prime factors of 13195 are 5, 7, 13 and 29.
//
// What is the largest prime factor of the number 600851475143 ?

package main

import (
	"fmt"
)

func isPrime(n int) bool {
	for i := 2; i < n; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

func main() {
	/// target := 1319500023
	target := 13195
	for element := 1; element < target; element++ {
		if isPrime(element) {
			fmt.Println(element)
		}
	}
}
