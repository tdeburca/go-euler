// Problem 5: Verbing
// Given a string, if its length is at least 3,
// add 'ing' to its end.
// Unless it already ends in 'ing', in which case
// add 'ly' instead.
// If the string length is less than 3, leave it unchanged.
// Return the resulting string.

package main

import (
	"fmt"
	"strings"
)

func verbing(s string) string {
	if len(s) > 2 {
		if strings.HasSuffix(s, "ing") {
			return s + "ly"
		}
		return s + "ing"
	}
	return s
}

func main() {
	fmt.Println(verbing("hail"))
	fmt.Println(verbing("swiming"))
	fmt.Println(verbing("do"))
}
