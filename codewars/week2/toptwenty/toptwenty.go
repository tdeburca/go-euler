package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

// splitOnNotLetter is a split function for a Scanner that returns each
// word of text, with surrounding spaces deleted. It will
// never return an empty string. The definition of space is set by
// unicode.IsLetter. (Based heavily on bufio.ScanWords)
func splitOnNotLetter(data []byte, atEOF bool) (advance int, token []byte, err error) {
	// Skip leading spaces.
	start := 0
	for width := 0; start < len(data); start += width {
		var r rune
		r, width = utf8.DecodeRune(data[start:])
		if unicode.IsLetter(r) {
			break
		}
	}
	// Scan until space, marking end of word.
	for width, i := 0, start; i < len(data); i += width {
		var r rune
		r, width = utf8.DecodeRune(data[i:])
		if !unicode.IsLetter(r) {
			return i + width, data[start:i], nil
		}
	}
	// If we're at EOF, we have a final, non-empty, non-terminated word. Return it.
	if atEOF && len(data) > start {
		return len(data), data[start:], nil
	}
	// Request more data.
	return start, nil, nil
}

func main() {
	file, err := os.Open("alice.txt")
	check(err)
	defer file.Close()

	words := make(map[string]int)

	scanner := bufio.NewScanner(file)
	// Set the split function to split on any non-letter character.
	scanner.Split(splitOnNotLetter)
	// scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		token := strings.ToLower(scanner.Text())
		outWord := token
		words[outWord] = words[outWord] + 1
	}

	// Now need list of scores, and the ability to lookup by score.
	scores := make(map[int][]string)
	for word, score := range words {
		// needs to be an array of words to handle multiple words with same score.
		scores[score] = append(scores[score], word)
	}

	// Create unique list of scores from map keys.
	scoreList := []int{}
	for key := range scores {
		scoreList = append(scoreList, key)
	}

	//Sort scores.
	sort.Ints(scoreList)

	// last index is one less than slice lenght
	max := len(scoreList) - 1
	// 20 is the number of results
	min := max - 20
	// 'Chart position',
	position := 1

	// Print scores.
	for i := max; i > min; i-- {
		fmt.Println(position, scoreList[i], scores[scoreList[i]])
		position++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
