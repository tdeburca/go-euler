package utilpkg

// Fib Returns the fibonacci secquence for all values less than n.
func Fib(n int) chan int {
	c := make(chan int)
	go func() {
		a, b := 0, 1
		// While b(last fib number) less than target.
		for b < n {
			a, b = b, a+b
			c <- a
		}
		close(c)
	}()
	return c
}
