// Problem 7: front_back
// Consider dividing a string into two halves.
// If the length is even, the front and back halves are the same length.
// If the length is odd, we'll say that the extra char goes in the front half.
// e.g. 'abcde', the front half is 'abc', the back half 'de'.
// Given 2 strings, a and b, return a string of the form
//  a-front + b-front + a-back + b-back

package main

import "fmt"

func frontBack(a, b string) string {
	afront, aback := splitString(a)
	bfront, bback := splitString(b)
	return afront + bfront + aback + bback
}

func splitString(s string) (string, string) {
	if len(s)%2 == 0 {
		return s[:len(s)/2], s[len(s)/2:]
	}
	return s[:len(s)/2+1], s[len(s)/2+1:]
}

func main() {
	fmt.Println(frontBack("abcd", "xy"))
	fmt.Println(frontBack("abcde", "xyz"))
	fmt.Println(frontBack("Kitten", "Donut"))
}
