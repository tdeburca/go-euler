// Write a program that opens a file and prints the number of lines in that file.

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

//This is how you declare a global constant
const filename string = "/Users/tdeburca/go/src/gitlab.com/tdeburca/go-euler/codewars/week2/alice.txt"

func main() {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()

	counter := 0
	scanner := bufio.NewScanner(file)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for scanner.Scan() {
		counter++
	}
	fmt.Printf("The number of lines in the file is: %d", counter)
}
