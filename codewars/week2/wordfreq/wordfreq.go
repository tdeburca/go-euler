package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	file, err := os.Open("alice.txt")
	check(err)
	defer file.Close()

	words := make(map[string]int)

	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Set the split function to split on any non-letter character.
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		// Can you make it case insensitive? (The == the?)
		outWord := strings.ToLower(scanner.Text())
		words[outWord] = words[outWord] + 1
	}

	//Can you order the output alphabetically?
	wordlist := []string{}

	for k := range words {
		wordlist = append(wordlist, k)
	}
	sort.Strings(wordlist)

	for word := range wordlist {
		currentWord := wordlist[word]
		fmt.Println(currentWord, words[currentWord])
	}

}
