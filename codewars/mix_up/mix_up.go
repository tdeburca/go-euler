// Problem 4
// Given strings a and b, return a single string with a and b separated
// by a space "<a> <b>", except swap the first 2 chars of each string.
// e.g.
//   "mix", pod" -> "pox mid"
//   "dog", "dinner" -> "dig donner"
// Assume a and b are length 2 or more.

package main

import "fmt"

func mixup(a, b string) string {
	return b[:2] + a[2:] + " " + a[:2] + b[2:]
}

func main() {
	fmt.Println(mixup("mix", "pod"))
	fmt.Println(mixup("dog", "dinner"))
	fmt.Println(mixup("gnash", "sport"))
	fmt.Println(mixup("pezzy", "firm"))
}
