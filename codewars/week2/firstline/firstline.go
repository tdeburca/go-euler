// Write a program that opens a file and prints the first line.

package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

//This is how you declare a global constant
const filename string = "/Users/tdeburca/go/src/gitlab.com/tdeburca/go-euler/codewars/week2/alice.txt"

func main() {
	file, err := os.Open(filename)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	//scanner.Split(bufio.ScanWords)
	if scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}
